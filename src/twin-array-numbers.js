const twinNumbersInArray = (array) => {
  // Your implementation

  if (array.length === 0 || !Array.isArray(array)) {
    return "Passed argument is not a array or empty";
  } else {
    let filtered = array.filter((el) => el % 2 === 0);

    if (filtered.length === 0) {
      return "Passed array not have twin numbers in array";
    } else {
      return filtered;
    }
  }
  // Read README.md file, if you not understand what to do
};

module.exports = twinNumbersInArray;
